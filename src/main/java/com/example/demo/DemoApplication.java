package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}


	/**
	 * This method creates different arrays to be solved.
	 * @return A string the sums and the output values of each sum.
	 */
	@GetMapping("/")
	public String Index()
	{
		String value1 = "{1,2,1} = " + this.MiddleIndex(new int[]{1,2,1}); // 1 (1,1)
		String value2 = "{5,4,1,9} = " + this.MiddleIndex(new int[]{5,4,1,9}); // 2 (5+4,9)
		String value3 = "{10, 1, 200, 2,5,4} = " + this.MiddleIndex( new int[]{10, 1, 200, 2,5,4}); // 2 (10+1,2+5+4)
		String value4 = "{12, 1, 200, 2,5,6} = " + this.MiddleIndex(new int[]{12, 1, 200, 2,5,6}); // 2 (12+1,2+5+6)

		return value1  + " || " + value2 + " || " + value3 + " || " + value4;
	}


	/**
	 * @param array array of integers
	 * @return The index of the input array (based on 0) such that the sum of the left-hand side is equal to the sum on
	 * the right-hand side
	 */
	public int MiddleIndex(int[] array) {
		int leftSum = 0;
		int rightSum = 0;
		for (int i = 0; i < array.length; i++) {
			rightSum += array[i];
		}
		for (int i = 0; i < array.length; i++) {
			rightSum -= array[i];
			if (leftSum == rightSum)
				return i;
			leftSum += array[i];
		}
		return -1;
	}
}